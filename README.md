<h1>Pour lancer le projet : </h1>
<ul>
    <li>Outil à installer sur votre machine : composer et sass</li>
    <li>Lancer la commande composer install</li>
    <li>Pour compiler les css node-sass bootstrap.scss ../css/bootstrap.css</li>
    <li>Si création de classes sup pensez à lancer un composer dump-autoload -o</li>
</ul>

<h1>Configuration Ldap : </h1>
<ul>
<li>Installer Ldap sur sa machine, (suivre juste l'étape 1.1) : https://www.debuntu.org/how-to-set-up-a-ldap-server-and-its-clients/</li>
<li>Modifier la ligne  suivante du ficher ldap.conf (etc/ldap/ldap.conf) : URI ldap://127.0.0.1/ </li>
<li>Installer l'outil apache Directory Studio qui sert à visualiser des fichier LDAP</li>
<li>Créer une nouvelle connexion dans l'outil avec les paramètres suivants : 
	<ul>
	<li><strong>Paramètres réseau</strong></li>
	<li>Nom d'hôte : 127.0.0.1</li>
	<li>port : 389</li>
	<li>Méthode d'encryption : Pas d'encryption</li>
	</ul>
	<ul>
	<li><strong>Authentification</strong></li>
	<li>Bind DN ou nom d'utilisateur : cn=admin,dc=localhost</li>
	<li>Mot de passe :  Le mot de passe crée lors de la config ldap</li>
	</ul>
</li>
</ul>


