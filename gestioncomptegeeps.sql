-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 20, 2018 at 04:46 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gestioncomptegeeps`
--

-- --------------------------------------------------------

--
-- Table structure for table `apourstatut`
--

CREATE TABLE `apourstatut` (
  `CODESTATUT` varchar(7) NOT NULL,
  `NOPERSONNE` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `axe`
--

CREATE TABLE `axe` (
  `CODEAXE` varchar(7) NOT NULL,
  `INTITULEAXE` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `compte`
--

CREATE TABLE `compte` (
  `NOCOMPTE` int(9) NOT NULL,
  `CODEPOLE` varchar(8) DEFAULT NULL,
  `LOGIN` varchar(80) DEFAULT NULL,
  `MDP` varchar(80) DEFAULT NULL,
  `NOM` varchar(80) DEFAULT NULL,
  `PRENOM` varchar(80) DEFAULT NULL,
  `EMAIL` varchar(80) DEFAULT NULL,
  `SUPERADMIN` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `compte`
--

INSERT INTO `compte` (`NOCOMPTE`, `CODEPOLE`, `LOGIN`, `MDP`, `NOM`, `PRENOM`, `EMAIL`, `SUPERADMIN`) VALUES
(5, 'ECO2', 'admin', '$2y$13$FJJI7DlrGtBlkUDQB0LoEuyz1bsay6mzkw5IWfCc2q3auhBtZ42Z6', 'Hubert', 'Olivier', 'gzrgzrgzr@gzrgzr.com', 1),
(1, 'PIEM', 'admin1piem', '$2y$13$iD0rq0r7DIYcll6HBLQ46eRudQwaoR92IopFjAKlUI/OITTb1m7Wi', 'Doo', 'John', 'doo.john@centrale.fr', 0),
(2, 'ECO2', 'admin2eco2', '$2y$13$FJJI7DlrGtBlkUDQB0LoEuyz1bsay6mzkw5IWfCc2q3auhBtZ42Z6', 'Alvarez', 'Rosa', 'rosa.alvarez@bomba.fr', 0),
(3, 'L2E', 'admin3l2e', '$2y$13$iD0rq0r7DIYcll6HBLQ46eRudQwaoR92IopFjAKlUI/OITTb1m7Wi', 'Armand', 'Thierry', 'a.thierry@u-psud.fr', 0),
(4, 'PHEMA', 'admin4phema', '$2y$13$iD0rq0r7DIYcll6HBLQ46eRudQwaoR92IopFjAKlUI/OITTb1m7Wi', 'Jesouis', 'Unolivier', 'je.olivier@aide.fr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contrat`
--

CREATE TABLE `contrat` (
  `NOCONTRAT` varchar(32) NOT NULL,
  `CODETYPECONTRAT` varchar(7) DEFAULT NULL,
  `CONTRAT` varchar(32) DEFAULT NULL,
  `SUJET2` varchar(32) DEFAULT NULL,
  `FINANCEMENT` varchar(32) DEFAULT NULL,
  `ECOLEDOCTORALE` varchar(32) DEFAULT NULL,
  `DIRECTEUR` varchar(32) DEFAULT NULL,
  `GESTIONNAIRE` varchar(32) DEFAULT NULL,
  `ORGANISMEDERATTACHEMENT` varchar(32) DEFAULT NULL,
  `GRATIFICATIONSALAIRE` tinyint(1) DEFAULT NULL,
  `NOSECURITESOCIALE` int(4) DEFAULT NULL,
  `DATEDEBUT` date DEFAULT NULL,
  `DATEFIN` date DEFAULT NULL,
  `CARTESEJOURDATEDEBUT` date DEFAULT NULL,
  `CARTESEJOURDATEFIN` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faitparti`
--

CREATE TABLE `faitparti` (
  `CODESERVICE` varchar(7) NOT NULL,
  `NOPERSONNE` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groupe_info`
--

CREATE TABLE `groupe_info` (
  `CODEGRPINFO` varchar(7) NOT NULL,
  `CODEHOMEDIRECTORY` varchar(7) DEFAULT NULL,
  `NOMGROUPE` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupe_info`
--

INSERT INTO `groupe_info` (`CODEGRPINFO`, `CODEHOMEDIRECTORY`, `NOMGROUPE`) VALUES
('PERM', NULL, 'Permanents'),
('DOCT', NULL, 'Doctorants'),
('PDOCT', NULL, 'Postdoctorants'),
('INVI', NULL, 'Invités'),
('STAG', NULL, 'Stagiaires');

-- --------------------------------------------------------

--
-- Table structure for table `homedirectory`
--

CREATE TABLE `homedirectory` (
  `CODEHOMEDIRECTORY` varchar(7) NOT NULL,
  `INTUTILEHOMEDIRECTORY` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homedirectory`
--

INSERT INTO `homedirectory` (`CODEHOMEDIRECTORY`, `INTUTILEHOMEDIRECTORY`) VALUES
('P', 'Permanents'),
('NP', 'Non Permanents');

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `CODEAXE` varchar(7) NOT NULL,
  `NOPERSONNE` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personne`
--

CREATE TABLE `personne` (
  `NOPERSONNE` int(9) NOT NULL,
  `CODEGRPINFO` varchar(7) DEFAULT NULL,
  `NOCONTRAT` varchar(80) DEFAULT NULL,
  `CODEPOLE` varchar(8) DEFAULT NULL,
  `NOPERSONNE_ESTRESPONSABLEDE` int(2) DEFAULT NULL,
  `NOM` varchar(80) DEFAULT NULL,
  `PRENOM` varchar(80) DEFAULT NULL,
  `EMAIL` varchar(80) DEFAULT NULL,
  `DATENAISSANCE` date DEFAULT NULL,
  `LIEUNAISSANCE` varchar(80) DEFAULT NULL,
  `TELEPHONEFIXE` varchar(80) DEFAULT NULL,
  `TELEPHONEMOBILE` varchar(80) DEFAULT NULL,
  `BUREAU` varchar(80) DEFAULT NULL,
  `BATIMENT` varchar(80) DEFAULT NULL,
  `TUTELLE` varchar(80) DEFAULT NULL,
  `ESTAUGEEPS` tinyint(1) DEFAULT NULL,
  `DATEARRIVE` date DEFAULT NULL,
  `DATEDEPART` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personne`
--

INSERT INTO `personne` (`NOPERSONNE`, `CODEGRPINFO`, `NOCONTRAT`, `CODEPOLE`, `NOPERSONNE_ESTRESPONSABLEDE`, `NOM`, `PRENOM`, `EMAIL`, `DATENAISSANCE`, `LIEUNAISSANCE`, `TELEPHONEFIXE`, `TELEPHONEMOBILE`, `BUREAU`, `BATIMENT`, `TUTELLE`, `ESTAUGEEPS`, `DATEARRIVE`, `DATEDEPART`) VALUES
(24, 'PERM', 'A89526', NULL, 1, 'Sofia', 'Gaiaschi', 'sofia@geeps.centralesupelec.fr', NULL, 'Evry', '145147486', '635256596', 'G002', 'GEEPS', 'Directeur', 2, '1999-12-02', NULL),
(25, NULL, NULL, 'Aucun', NULL, 'Jinane', 'Harmouche', 'gauthier@geeps.centralesupelec.fr', NULL, NULL, '16856979', NULL, 'B995   ', 'BULL', 'Directeur  ', NULL, '2018-06-14', NULL),
(21, 'INVI', 'A880', NULL, 1, 'Gauthier', 'Deplaude', 'gauthier@geeps.centralesupelec.fr', NULL, 'Oran', '15256526', '65252526', 'B451', 'BALO', 'Krutchev', 1, '2017-06-23', NULL),
(22, 'PDOCT', 'A7412', NULL, 1, 'Matthias', 'Elbaz', 'elbaz@geeps.centralesupelec.fr', NULL, 'Ivry', '174212521', '674121011', 'B003', 'BULL', 'Olivier Hubert', 2, '2016-12-16', NULL),
(23, 'DOCT', 'A5266', NULL, 2, 'Fadhel', 'Siwar', 'siwar@geeps.centralesupelec.fr', NULL, 'Porto', '145256556', '636562564', 'B999', 'BULL', 'Directeur', 2, '2001-12-15', NULL),
(19, NULL, NULL, 'Aucun', NULL, 'David', 'Alamarguy', 'david@geeps.centralesupelec.fr', NULL, NULL, '16856979', NULL, 'B995        ', 'BULL', 'Directeur       ', NULL, '2018-06-12', NULL),
(3, 'INVI', 'A879', NULL, 0, 'Marchand', 'Yasmine', 'afbuoaeie@bngze.com', NULL, 'Oran', '15256525', '65252525', 'B450', 'BALO', 'Krutchev', 0, '2017-06-22', NULL),
(5, 'PDOCT', 'A7411', NULL, 0, 'Leveterant', 'Henry', 'aeageae@aegaeg.com', NULL, 'Ivry', '174212520', '674121010', 'B002', 'BULL', 'Olivier Hubert', 1, '2016-12-15', NULL),
(6, 'DOCT', 'A5265', NULL, 1, 'Alvarez', 'Rosa', 'rosa3@mail.com', NULL, 'Porto', '145256555', '636562563', 'B998', 'BULL', 'Directeur', 1, '2001-12-14', NULL),
(7, 'PERM', 'A89525', NULL, 0, 'Hubert', 'Olivier', 'ergergerg@mail.com', NULL, 'Evry', '145147485', '635256595', 'G001', 'GEEPS', 'Directeur', 1, '1999-12-01', NULL),
(26, 'INVI', 'A881', NULL, 2, 'Jean-Paul', 'Kleider', 'jean-Paul@geeps.centralesupelec.fr', NULL, 'Oran', '15256527', '65252527', 'B452', 'BALO', 'Krutchev', 2, '2017-06-24', NULL),
(27, 'PDOCT', 'A7413', 'Aucun', 2, 'Alexander', 'Korovin', 'alexander@geeps.centralesupelec.fr', NULL, 'Ivry', '174212522', '674121012', 'B004 ', 'BULL', 'Olivier Hubert ', 3, '2016-12-17', NULL),
(28, 'DOCT', 'A5267', NULL, 3, 'Gwendoline', 'Lefebvre', 'gwendoline@geeps.centralesupelec.fr', NULL, 'Porto', '145256557', '636562565', 'B1000', 'BULL', 'Directeur', 3, '2001-12-16', NULL),
(29, 'PERM', 'A89527', NULL, 2, 'Mai', 'Vanhuy', 'vanhuy@geeps.centralesupelec.fr', NULL, 'Evry', '145147487', '635256597', 'G003', 'GEEPS', 'Directeur', 3, '1999-12-03', NULL),
(30, NULL, NULL, 'Aucun', NULL, 'Mohamed', 'Ouameur', 'mohamed@geeps.centralesupelec.fr', NULL, NULL, '16856980', NULL, 'B995   ', 'BULL', 'Directeur  ', NULL, '2018-06-15', NULL),
(31, 'INVI', 'A882', NULL, 3, 'Lionel', 'Pichon', 'pichon@geeps.centralesupelec.fr', NULL, 'Oran', '15256528', '65252528', 'B453', 'BALO', 'Krutchev', 3, '2017-06-25', NULL),
(32, 'PDOCT', 'A7414', NULL, 3, 'Puspitosari', 'Nastiti', 'puspitosari@geeps.centralesupelec.fr', NULL, 'Ivry', '174212523', '674121013', 'B005', 'BULL', 'Olivier Hubert', 4, '2016-12-18', NULL),
(33, 'DOCT', 'A5268', NULL, 4, 'Emmanuel', 'Odic', 'odic@geeps.centralesupelec.fr', NULL, 'Porto', '145256558', '636562566', 'B1001', 'BULL', 'Directeur', 4, '2001-12-17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pole`
--

CREATE TABLE `pole` (
  `CODEPOLE` varchar(8) NOT NULL,
  `NOPERSONNE` int(2) DEFAULT NULL,
  `INTITULEPOLE` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pole`
--

INSERT INTO `pole` (`CODEPOLE`, `NOPERSONNE`, `INTITULEPOLE`) VALUES
('PIEM', NULL, 'piem'),
('PHEMADIC', NULL, 'phemadic'),
('ECO2', NULL, 'eco2'),
('L2E', NULL, 'l2e');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `CODESERVICE` varchar(7) NOT NULL,
  `NOPERSONNE` int(2) DEFAULT NULL,
  `INTITULESERVICE` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`CODESERVICE`, `NOPERSONNE`, `INTITULESERVICE`) VALUES
('ADMFIN', NULL, 'administratif et financier'),
('INFO', NULL, 'ressources informatiques'),
('HYGSEC', NULL, 'hygiene et securite'),
('INFRA', NULL, 'infrastructure'),
('MECA', NULL, 'mecanique'),
('ELECT', NULL, 'electronique'),
('INSTRU', NULL, 'instrumentation'),
('CALSCI', NULL, 'calcul scientifique'),
('RECH', NULL, 'recherche'),
('NONE', NULL, 'none');

-- --------------------------------------------------------

--
-- Table structure for table `soustheme`
--

CREATE TABLE `soustheme` (
  `CODESOUS_THEME` varchar(7) NOT NULL,
  `CODETHEME` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sous_theme`
--

CREATE TABLE `sous_theme` (
  `CODESOUS_THEME` varchar(7) NOT NULL,
  `INTITULESOUS_THEME` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statut`
--

CREATE TABLE `statut` (
  `CODESTATUT` varchar(7) NOT NULL,
  `NOMSTATUT` varchar(32) DEFAULT NULL,
  `NOMSTATUTENGLISH` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statut`
--

INSERT INTO `statut` (`CODESTATUT`, `NOMSTATUT`, `NOMSTATUTENGLISH`) VALUES
('ADJAD', 'Adjoint administratif', 'Administrative Adjoint'),
('AINGE', 'Apprenti ingénieur', 'Engineer Apprentice'),
('ALICE', 'Apprenti licence', 'Licence Apprentice'),
('AMAST', 'Apprenti master', 'Master Apprentice'),
('ATECH', 'Apprenti technicien', 'Technician Apprentice'),
('ASING', 'Assistant ingenieur', 'Engineer Assistant'),
('CRCNRS', 'Chargé de recherche CNRS', 'Research charged CNRS'),
('CHERCHA', 'Chercheur associé', 'Associated Researcher'),
('CONSUL', 'Consultant', 'Consultant'),
('DRCNRS', 'Directeur de recherche CNRS', 'Research Director CNRS'),
('DOCTO', 'Doctorant', 'PhD student'),
('IETUD', 'Ingenieur d\'études', 'Engineer'),
('IRECH', 'Ingenieur de recherche', 'Research Engineer'),
('INVIT', 'Invité', 'Guest'),
('MCONF', 'Maitre de conférence', 'Assistant Professor'),
('PLEXT', 'Personnel externe', 'External Personal'),
('PDOCT', 'Post-doctorant', 'Post-doctoral'),
('PGRAD', 'Post-gradué', 'Post-graduate'),
('PROFU', 'Professeur des universités', 'Professor'),
('PROF', 'Professeur émérite', 'University Professor'),
('STAG', 'Stagiaire', 'Training'),
('TECH', 'Technicien', 'Technician');

-- --------------------------------------------------------

--
-- Table structure for table `thematique`
--

CREATE TABLE `thematique` (
  `CODEAXE` varchar(7) NOT NULL,
  `CODETHEME` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `CODETHEME` varchar(7) NOT NULL,
  `CODEPOLE` varchar(7) DEFAULT NULL,
  `INTITULETHEME` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `type_contrat`
--

CREATE TABLE `type_contrat` (
  `CODETYPECONTRAT` varchar(7) NOT NULL,
  `INTITULETYPECONTRAT` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apourstatut`
--
ALTER TABLE `apourstatut`
  ADD PRIMARY KEY (`CODESTATUT`,`NOPERSONNE`),
  ADD KEY `I_FK_APOURSTATUT_STATUT` (`CODESTATUT`),
  ADD KEY `I_FK_APOURSTATUT_PERSONNE` (`NOPERSONNE`);

--
-- Indexes for table `axe`
--
ALTER TABLE `axe`
  ADD PRIMARY KEY (`CODEAXE`);

--
-- Indexes for table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`NOCOMPTE`),
  ADD KEY `I_FK_COMPTE_POLE` (`CODEPOLE`);

--
-- Indexes for table `contrat`
--
ALTER TABLE `contrat`
  ADD PRIMARY KEY (`NOCONTRAT`),
  ADD KEY `I_FK_CONTRAT_TYPE_CONTRAT` (`CODETYPECONTRAT`);

--
-- Indexes for table `faitparti`
--
ALTER TABLE `faitparti`
  ADD PRIMARY KEY (`CODESERVICE`,`NOPERSONNE`),
  ADD KEY `I_FK_FAITPARTI_SERVICE` (`CODESERVICE`),
  ADD KEY `I_FK_FAITPARTI_PERSONNE` (`NOPERSONNE`);

--
-- Indexes for table `groupe_info`
--
ALTER TABLE `groupe_info`
  ADD PRIMARY KEY (`CODEGRPINFO`),
  ADD KEY `I_FK_GROUPE_INFO_HOMEDIRECTORY` (`CODEHOMEDIRECTORY`);

--
-- Indexes for table `homedirectory`
--
ALTER TABLE `homedirectory`
  ADD PRIMARY KEY (`CODEHOMEDIRECTORY`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`CODEAXE`,`NOPERSONNE`),
  ADD KEY `I_FK_PARTICIPANT_AXE` (`CODEAXE`),
  ADD KEY `I_FK_PARTICIPANT_PERSONNE` (`NOPERSONNE`);

--
-- Indexes for table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`NOPERSONNE`),
  ADD KEY `I_FK_PERSONNE_GROUPE_INFO` (`CODEGRPINFO`),
  ADD KEY `I_FK_PERSONNE_CONTRAT` (`NOCONTRAT`),
  ADD KEY `I_FK_PERSONNE_PERSONNE` (`NOPERSONNE_ESTRESPONSABLEDE`),
  ADD KEY `I_FK_PERSONNE_POLE` (`CODEPOLE`);

--
-- Indexes for table `pole`
--
ALTER TABLE `pole`
  ADD PRIMARY KEY (`CODEPOLE`),
  ADD KEY `I_FK_POLE_PERSONNE` (`NOPERSONNE`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`CODESERVICE`),
  ADD KEY `I_FK_SERVICE_PERSONNE` (`NOPERSONNE`);

--
-- Indexes for table `soustheme`
--
ALTER TABLE `soustheme`
  ADD PRIMARY KEY (`CODESOUS_THEME`,`CODETHEME`),
  ADD KEY `I_FK_SOUSTHEME_SOUS_THEME` (`CODESOUS_THEME`),
  ADD KEY `I_FK_SOUSTHEME_THEME` (`CODETHEME`);

--
-- Indexes for table `sous_theme`
--
ALTER TABLE `sous_theme`
  ADD PRIMARY KEY (`CODESOUS_THEME`);

--
-- Indexes for table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`CODESTATUT`);

--
-- Indexes for table `thematique`
--
ALTER TABLE `thematique`
  ADD PRIMARY KEY (`CODEAXE`,`CODETHEME`),
  ADD KEY `I_FK_THEMATIQUE_AXE` (`CODEAXE`),
  ADD KEY `I_FK_THEMATIQUE_THEME` (`CODETHEME`);

--
-- Indexes for table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`CODETHEME`),
  ADD KEY `I_FK_THEME_POLE` (`CODEPOLE`);

--
-- Indexes for table `type_contrat`
--
ALTER TABLE `type_contrat`
  ADD PRIMARY KEY (`CODETYPECONTRAT`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `compte`
--
ALTER TABLE `compte`
  MODIFY `NOCOMPTE` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personne`
--
ALTER TABLE `personne`
  MODIFY `NOPERSONNE` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;