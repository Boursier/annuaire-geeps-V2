<?php

/**
 * Created by PhpStorm.
 * User: Yannous
 * Date: 20/03/2018
 * Time: 14:15
 */

namespace App;

class LdapClass {

    private $_connexion;
    private $_ldap_bind;

    public function __construct() {
        $ldaphost = "ldap://127.0.0.1:389";
        $this->_connexion = ldap_connect($ldaphost)
                or die("Impossible de se connecter au serveur LDAP {$ldaphost}");
        ldap_set_option($this->_connexion, LDAP_OPT_PROTOCOL_VERSION, 3);
        $this->_ldap_bind = ldap_bind($this->_connexion, "cn=admin,dc=localhost", "admin");
    }

    public function ajouterPersonneLdap() {
        $info = array();
        $info["cn"] = $_POST['nom'];
        $info["telephoneNumber"] = $_POST['telFixe'];
        $info["userPassword"] = "0000000";
        $info["objectClass"][0] = "simpleSecurityObject";
        $info["objectClass"][1] = "organizationalRole";
        try {
            ldap_add($this->_connexion, "cn=" . $info["cn"] . ",dc=localhost", $info);
            return true;
        } catch (Exception $e) {
            print "Erreur Ldap!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function supprimerPersonneLdap() {
        $info = array();
        $info["cn"] = $_POST['nom'];
        try {
            ldap_delete($this->_connexion, "cn=" . $info["cn"] . ",dc=localhost");
        } catch (Exception $e) {
            print "Erreur Ldap!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function modifierPersonneLdap() {
        $info = array();
        $info["cn"] = $_POST['nom'];
        $info["telephoneNumber"] = $_POST['telFixe'];
        try {
            ldap_modify($this->_connexion, "cn=" . $info["cn"] . ",dc=localhost", $info);
            return true;
        } catch (Exception $e) {
            print "Erreur Ldap!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function __destruct() {
        //ldap_unbind($this->_connexion);
        ldap_close($this->_connexion);
    }

}
