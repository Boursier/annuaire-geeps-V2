<?php
/**
 * Created by PhpStorm.
 * User: Yannous
 * Date: 20/03/2018
 * Time: 14:00
 */
namespace App;
use PDO;
use DateTime;

class BddClass
{
    /**
     * @var PDO
     */
    private $_bdd;

    /**
     * BddClass constructor.
     */
    public function __construct()
    {
        try{
            $this->_bdd = new PDO(BDD, BDD_UTILISATEUR, BDD_MOTDEPASSE);
        }
        catch (PDOException $e)
        {
            print "Erreur Bdd!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /* ------------------------------------------------
                   connexion.php
   ------------------------------------------------ */

    /**
     * @param $login
     * @param $mdp
     * @return bool
     */
    function identification($login, $mdp)
    {
        $req = $this->_bdd->prepare("SELECT MDP FROM compte WHERE login = :login");
        $req->bindParam(":login", $login);
        $req->execute();
        $res = $req->fetchall();
        if($req->rowCount() > 0 && password_verify($mdp, $res[0][0]) === true)
        {
            $req = $this->_bdd->prepare("SELECT NOCOMPTE, CODEPOLE, LOGIN, NOM, PRENOM, EMAIL FROM compte WHERE login = :login");
            $req->bindParam(":login", $login);
            $req->execute();
            $res = $req->fetchall();
            $_SESSION['nocompte'] = $res[0][0];
            $_SESSION['pole'] = $res[0][1];
            $_SESSION['login'] = $res[0][2];
            $_SESSION['nom'] = $res[0][3];
            $_SESSION['prenom'] = $res[0][4];
            $_SESSION['email'] = $res[0][5];

            return true;
        }
        else
        {
            return false;
        }
    }

    /* ------------------------------------------------
                   personne.php
   ------------------------------------------------ */

    /**
     * @return array
     */
    function listePoles()
    {
        $req = $this->_bdd->prepare("SELECT CODEPOLE FROM pole");
        $req->execute();
        return $req->fetchall();

    }

    /**
     * @return array
     */
    function infosPersonne()
    {
        $req = $this->_bdd->prepare("SELECT NOPERSONNE, NOM, PRENOM, EMAIL, TELEPHONEFIXE, DATEARRIVE, TELEPHONEMOBILE, CODEPOLE, BUREAU, BATIMENT, NOCONTRAT, TUTELLE, DATENAISSANCE, LIEUNAISSANCE, DATEDEPART FROM personne WHERE NOPERSONNE=:noPersonne");
        $req->bindParam(":noPersonne",$_POST['noPersonne']);
        $req->execute();
        return $req->fetchall();
    }

    /**
     * @return bool
     */
    function modifierPersonne()
    {
        $req = $this->_bdd->prepare("UPDATE personne SET NOM=:nom, PRENOM=:prenom, EMAIL=:email, TELEPHONEFIXE=:telFixe, DATEARRIVE=:dateArrive, TELEPHONEMOBILE=:telPort, CODEPOLE=:pole, BUREAU=:bureau, BATIMENT=:batiment, NOCONTRAT=:noContrat, TUTELLE=:tutelle, DATENAISSANCE=:dateNaiss, LIEUNAISSANCE=:lieuNaiss, DATEDEPART=:dateDepart WHERE NOPERSONNE=:noPersonne");

        // On convertit les chaînes vides de $_POST en null pour pouvoir les insérer dans la BDD
        $_POST = array_map(function($value) {
            return empty($value) === true ? null : $value;
        }, $_POST);

        $req->bindParam(":noPersonne",$_POST['noPersonne']);
        $req->bindParam(":nom",$_POST['nom']);
        $req->bindParam(":prenom",$_POST['prenom']);
        $req->bindParam(":email",$_POST['email']);
        $req->bindParam(":telFixe",$_POST['telFixe']);

        if(empty($_POST['dateArrive']) === false)
        {
            $dateArrive = DateTime::createFromFormat('d/m/Y', $_POST['dateArrive']);
            if($dateArrive === false)
            {
                return false;
            }
            $dateArrive = $dateArrive->format('Y-m-d');
        }
        $req->bindParam(":dateArrive",$dateArrive);

        $req->bindParam(":telPort",$_POST['telPort']);
        $req->bindParam(":pole",$_POST['pole']);
        $req->bindParam(":bureau",$_POST['bureau']);
        $req->bindParam(":batiment",$_POST['batiment']);
        $req->bindParam(":noContrat",$_POST['noContrat']);
        $req->bindParam(":tutelle",$_POST['tutelle']);

        if(empty($_POST['dateNaiss']) === false)
        {
            $dateNaiss = DateTime::createFromFormat('d/m/Y', $_POST['dateNaiss']);
            if($dateNaiss === false)
            {
                return false;
            }
            $dateNaiss = $dateNaiss->format('Y-m-d');
        }
        $req->bindParam(":dateNaiss",$dateNaiss);

        $req->bindParam(":lieuNaiss",$_POST['lieuNaiss']);

        if(empty($_POST['dateDepart']) === false)
        {
            $dateDepart = DateTime::createFromFormat('d/m/Y', $_POST['dateDepart']);
            if($dateDepart === false)
            {
                return false;
            }
            $dateDepart = $dateDepart->format('Y-m-d');
        }
        $req->bindParam(":dateDepart",$dateDepart);

        $req->execute();

        return true;
    }

    /**
     * @return bool
     */
    function ajouterPersonne()
    {
        $req = $this->_bdd->prepare("INSERT INTO personne (NOM, PRENOM, EMAIL, TELEPHONEFIXE, DATEARRIVE, TELEPHONEMOBILE, CODEPOLE, BUREAU, BATIMENT, NOCONTRAT, TUTELLE, DATENAISSANCE, LIEUNAISSANCE, DATEDEPART) VALUES (:nom, :prenom, :email, :telFixe, :dateArrive, :telPort, :pole, :bureau, :batiment, :noContrat, :tutelle, :dateNaiss, :lieuNaiss, :dateDepart)");

        // On convertit les chaînes vides de $_POST en null pour pouvoir les insérer dans la BDD
        $_POST = array_map(function($value) {
            return empty($value) === true ? null : $value;
        }, $_POST);

        $req->bindParam(":nom",$_POST['nom']);
        $req->bindParam(":prenom",$_POST['prenom']);
        $req->bindParam(":email",$_POST['email']);
        $req->bindParam(":telFixe",$_POST['telFixe']);

        if(empty($_POST['dateArrive']) === false)
        {
            $dateArrive = DateTime::createFromFormat('d/m/Y', $_POST['dateArrive']);
            if($dateArrive === false)
            {
                return false;
            }
            $dateArrive = $dateArrive->format('Y-m-d');
        }
        $req->bindParam(":dateArrive",$dateArrive);

        $req->bindParam(":telPort",$_POST['telPort']);
        $req->bindParam(":pole",$_POST['pole']);
        $req->bindParam(":bureau",$_POST['bureau']);
        $req->bindParam(":batiment",$_POST['batiment']);
        $req->bindParam(":noContrat",$_POST['noContrat']);
        $req->bindParam(":tutelle",$_POST['tutelle']);

        if(empty($_POST['dateNaiss']) === false)
        {
            $dateNaiss = DateTime::createFromFormat('d/m/Y', $_POST['dateNaiss']);
            if($dateNaiss === false)
            {
                return false;
            }
            $dateNaiss = $dateNaiss->format('Y-m-d');
        }
        $req->bindParam(":dateNaiss",$dateNaiss);

        $req->bindParam(":lieuNaiss",$_POST['lieuNaiss']);

        if(empty($_POST['dateDepart']) === false)
        {
            $dateDepart = DateTime::createFromFormat('d/m/Y', $_POST['dateDepart']);
            if($dateDepart === false)
            {
                return false;
            }
            $dateDepart = $dateDepart->format('Y-m-d');
        }
        $req->bindParam(":dateDepart",$dateDepart);

        $req->execute();
        
        /* Permet au gestionnaire ldap d'inserer le même id*/
        $_POST['NOPERSONNE'] = $this->_bdd->lastInsertId(); 
        return true;
    }

    /* ------------------------------------------------
                   gestion.php
   ------------------------------------------------ */

    /**
     * @return array
     */
    function listePersonnes()
    {
        $req = $this->_bdd->prepare("SELECT NOPERSONNE, NOM, PRENOM, EMAIL, DATENAISSANCE, LIEUNAISSANCE, TELEPHONEFIXE, TELEPHONEMOBILE, BUREAU, BATIMENT, NOCONTRAT, TUTELLE, DATEARRIVE, DATEDEPART FROM personne ORDER BY NOM asc");
        $req->execute();
        return $req->fetchall();
    }

    /**
     * @return bool
     */
    function supprimerPersonne()
    {
        $req = $this->_bdd->prepare("DELETE FROM personne WHERE NOPERSONNE=:noPersonne");
        $req->bindParam(":noPersonne",$_POST['noPersonne']);
        $req->execute();

        return true;
    }

    /**
     * @return array
     */
    function infosCompte()
    {
        $req = $this->_bdd->prepare("SELECT CODEPOLE, LOGIN, NOM, PRENOM, EMAIL, SUPERADMIN FROM compte WHERE NOCOMPTE=:nocompte");
        $req->bindParam(":nocompte",$_SESSION['nocompte']);
        $req->execute();
        $res = $req->fetchall();

        $_SESSION['pole'] = $res[0][0];
        $_SESSION['login'] = $res[0][1];
        $_SESSION['nom'] = $res[0][2];
        $_SESSION['prenom'] = $res[0][3];
        $_SESSION['email'] = $res[0][4];
        $_SESSION['role'] = $res[0][5];

        return $res;
    }

    /* ------------------------------------------------
                       compte.php
       ------------------------------------------------ */

    /**
     * @return bool
     */
    function modifierCompte()
    {
        $req = $this->_bdd->prepare("UPDATE compte SET CODEPOLE=:pole, LOGIN=:login, MDP=:mdp, NOM=:nom, PRENOM=:prenom, EMAIL=:email WHERE NOCOMPTE=:nocompte");
        $req->bindParam(":nocompte",$_SESSION['nocompte']);
        $req->bindParam(":pole",$_POST['pole']);
        $req->bindParam(":login",$_POST['login']);
        $mdp = password_hash($_POST['mdp1'],PASSWORD_BCRYPT,["cost" => 13]);
        $req->bindParam(":mdp",$mdp);
        $req->bindParam(":nom",$_POST['nom']);
        $req->bindParam(":prenom",$_POST['prenom']);
        $req->bindParam(":email",$_POST['email']);
        $req->execute();

        return true;
    }
}


