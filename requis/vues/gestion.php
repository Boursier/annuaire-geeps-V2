
<div class="air" class="col-md-12">
    <?php if ($_SESSION['role'] == 1): ?>
    <a href="index.php?page=personne" class="btn btn btn-outline-primary">Ajouter une personne</a>
     <?php endif ?>
    <a href="index.php?page=exporter" class="btn btn-outline-primary">Exporter</a>
    <!--
    <form>
        <input type="file" name="file" id="file" class="btn btn-outline-primary"/> 
        <input type="hidden" name="MAX_FILE_SIZE" value="100000">
        Fichier : <input type="file" name="avatar">
        <input type="submit" name="envoyer" value="Envoyer le fichier">
    </form>
    !-->

</div>


<style>
    @media screen and (max-width:980px){
        tr {
            word-break: break-all !important;
        } 
    }
</style>
<div id="no-more-tables" class="">
    <style>
        
    </style>
    <table id="myTable" class="table">

        <thead class="thead-dark">
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th style="width:20%;">Email</th>
                <th style="width:20%;">Téléphone fixe</th>
                <th>Bureau</th>
                <th>Bâtiment</th>
                <th>Tutelle</th>
                <th style="width:30%;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($resListePersonnes as $r) {
                if (empty($r['DATEARRIVE']) === false) {
                    $r['DATEARRIVE'] = DateTime::createFromFormat('Y-m-d', $r['DATEARRIVE']);
                    $r['DATEARRIVE'] = $r['DATEARRIVE']->format('d/m/Y');
                }
                if (empty($r['DATENAISSANCE']) === false) {
                    $r['DATENAISSANCE'] = DateTime::createFromFormat('Y-m-d', $r['DATENAISSANCE']);
                    $r['DATENAISSANCE'] = $r['DATENAISSANCE']->format('d/m/Y');
                }
                if (empty($r['DATEDEPART']) === false) {
                    $r['DATEDEPART'] = DateTime::createFromFormat('Y-m-d', $r['DATEDEPART']);
                    $r['DATEDEPART'] = $r['DATEDEPART']->format('d/m/Y');
                }

                $r = array_map(function($value) {
                    return empty($value) === true ? "Non spécifié" : $value;
                }, $r);
                ?>
                <tr>
                    <td data-title="Nom"><?php echo htmlspecialchars($r['NOM'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Prénom"><?php echo htmlspecialchars($r['PRENOM'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Email"><?php echo htmlspecialchars($r['EMAIL'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Téléphone fixe"><?php echo htmlspecialchars($r['TELEPHONEFIXE'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Bureau"><?php echo htmlspecialchars($r['BUREAU'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Bâtiment"><?php echo htmlspecialchars($r['BATIMENT'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Tutelle"><?php echo htmlspecialchars($r['TUTELLE'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td data-title="Action">

                        <form class="formBoutons" method="post" action="index.php?page=personne">
                            <input type="hidden" name="modifier" value=1>
                            <input type="hidden" name="noPersonne" value=<?php echo htmlspecialchars($r['NOPERSONNE'], ENT_QUOTES, 'UTF-8'); ?>>
                            <button class="btn btn-sm btn-primary" type="submit" value="Modifier">
                                <i class="fas fa-eye"></i>
                            </button>
                        </form>


                        <?php if ($_SESSION['role'] == 1): ?>
                            <form class="formBoutons" method="post" action="index.php?page=personne">
                                <input type="hidden" name="modifier" value=1>
                                <input type="hidden" name="noPersonne" value=<?php echo htmlspecialchars($r['NOPERSONNE'], ENT_QUOTES, 'UTF-8'); ?>>
                                <button class="btn btn-success btn-sm" type="submit" value="Modifier">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </form>

                            <form class="formBoutons" method="post" action="index.php?page=gestion">
                                <input type="hidden" name="supprimer" value=1>
                                  <input type="hidden" name="nom" value=<?php echo htmlspecialchars($r['NOM'], ENT_QUOTES, 'UTF-8'); ?>>
                                <input type="hidden" name="noPersonne" value=<?php echo htmlspecialchars($r['NOPERSONNE'], ENT_QUOTES, 'UTF-8'); ?>>
                                <button class="btn btn-danger btn-sm" type="submit" value="Supprimer">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>

                        <?php endif; ?>



                        <form class="formBoutons" method="post" action="index.php?page=export_personne">
                            <input type="hidden" name="export_personne" value=1>
                            <input type="hidden" name="noPersonne" value=<?php echo htmlspecialchars($r['NOPERSONNE'], ENT_QUOTES, 'UTF-8'); ?>>
                            <button class="btn btn-secondary btn-sm" type="submit" value="exporter_personne">
                                <i class="fas fa-download"></i>
                            </button>
                        </form>
                    </td>
                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>

    <?php include('requis/footer.php'); ?>

    <script>
        $(document).ready(function () {
            var table = $('#myTable').DataTable({
                "language": {
                    "lengthMenu": "Affichage de _MENU_ lignes par page",
                    "zeroRecords": "Aucune donnée à afficher",
                    "info": "Page(s) _PAGE_ de _PAGES_",
                    "infoEmpty": "Champ vide",
                    "infoFiltered": "(filtré sur _MAX_ de lignes au total)",
                    "search": "Rechercher : ",
                    "paginate": {
                        "first": "Premier",
                        "last": "Dernier",
                        "next": "Suivant",
                        "previous": "Précédent"
                    }
                }
            });
            //$('div.dataTables_filter input').addClass('whatever');

            $('#datatable tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
            });
        });
    </script>