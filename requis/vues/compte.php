<form class="form-signin" method="post">
    <span class="messageConfirmation">
        <?php
        if (empty($_SESSION['succesCompte']) === false) {
            echo htmlspecialchars($_SESSION['succesCompte'], ENT_QUOTES, 'UTF-8') . "<br>";
            unset($_SESSION['succesCompte']);
        }
        ?>
    </span>
    <span class="messageErreur">
        <?php
        if (empty($_SESSION['erreurCompte']) === false) {
            echo htmlspecialchars($_SESSION['erreurCompte'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['erreurCompte']);
        }
        ?>
    </span>
    <fieldset>
        <legend>Modification de compte </legend>
        <label>Nom :</label><input type="text" class="form-control" name="nom" value="<?php echo htmlspecialchars($_SESSION['nom'], ENT_QUOTES, 'UTF-8'); ?>" placeholder="Nom" required>
        <label>Prénom :</label><input type="text" class="form-control" name="prenom" value="<?php echo htmlspecialchars($_SESSION['prenom'], ENT_QUOTES, 'UTF-8'); ?>" placeholder="Prénom" required>
        <label>Email :</label><input type="email" class="form-control" name="email" value="<?php echo htmlspecialchars($_SESSION['email'], ENT_QUOTES, 'UTF-8'); ?>" placeholder="Email" required>
        <label>Login :</label><input type="text" class="form-control" name="login" value="<?php echo htmlspecialchars($_SESSION['login'], ENT_QUOTES, 'UTF-8'); ?>" placeholder="Login" required>
        <label>Mot de passe :</label><input type="password" class="form-control" name="mdp1" placeholder="Mot de passe" required>
        <label>Confirmation du mot de passe :</label><input type="password" class="form-control" name="mdp2" placeholder="Confirmation du mot de passe" required>
        <br>
        <label class="lbSelect">Pôle :</label>
        <select name="pole" required>
            <?php
            foreach ($resListePoles as $r) {
                ?>
                <option value="<?php echo htmlspecialchars($r['CODEPOLE'], ENT_QUOTES, 'UTF-8'); ?>" <?php
                if ($r['CODEPOLE'] == $_SESSION['pole']) {
                    echo "selected";
                }
                ?>><?php echo htmlspecialchars($r['CODEPOLE'], ENT_QUOTES, 'UTF-8'); ?></option>
                <?php
            }
            ?>
        </select>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Enregistrer</button>
        <a href="index.php?page=gestion" class="btn btn-lg btn-primary btn-block">Retour</a>
    </fieldset>
</form>