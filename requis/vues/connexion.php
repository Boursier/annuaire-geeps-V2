<form class="form-signin" method="post">
    
    <div class="text-center">
        <img class="mb-4" src="img/logo-geeps.png" alt="" width="250">
        <h1 class="h3 mb-3 font-weight-normal">Identification</h1>
    </div>
    
    <span class="messageErreur">
        <?php
        if (empty($_SESSION['erreurIdentification']) === false) {
            echo htmlspecialchars($_SESSION['erreurIdentification'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['erreurIdentification']);
        }
        ?>
    </span>

    <div class="form-group">
        <input type="text" class="form-control" name="login" placeholder="Login" required="" autofocus="">
    </div>

    <div class="form-group">
        <input type="password" class="form-control" name="mdp" placeholder="Mot de passe" required="">
    </div>
    
    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Connexion">
    <?php
    $token = md5(bin2hex(openssl_random_pseudo_bytes(5)));
    $_SESSION['token'] = $token;
    echo '<input type="hidden" name="token" value="' . $token . '">';
    ?>
    
</form>