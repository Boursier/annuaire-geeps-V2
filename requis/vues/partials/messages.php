<?php

function getOldValue($res) {
    return htmlspecialchars($res, ENT_QUOTES, 'UTF-8');
}

if ($action == "modifier") {

    foreach ($resInfosPersonne as $res) {
        if (empty($res['DATEARRIVE']) === false) {
            $res['DATEARRIVE'] = DateTime::createFromFormat('Y-m-d', $res['DATEARRIVE']);
            $res['DATEARRIVE'] = $res['DATEARRIVE']->format('d/m/Y');
        }

        if (empty($res['DATENAISSANCE']) === false) {
            $res['DATENAISSANCE'] = DateTime::createFromFormat('Y-m-d', $res['DATENAISSANCE']);
            $res['DATENAISSANCE'] = $res['DATENAISSANCE']->format('d/m/Y');
        }

        if (empty($res['DATEDEPART']) === false) {
            $res['DATEDEPART'] = DateTime::createFromFormat('Y-m-d', $res['DATEDEPART']);
            $res['DATEDEPART'] = $res['DATEDEPART']->format('d/m/Y');
        }
    }
    ?>

    <span class="messageConfirmation">
        <?php
        if (!empty($_SESSION['succesModif'])) {
            echo "<br>" . htmlspecialchars($_SESSION['succesModif'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['succesModif']);
        }
        ?>
    </span>
    <span class="messageErreur">
        <?php
        if (!empty($_SESSION['erreurModif'])) {
            echo "<br>" . htmlspecialchars($_SESSION['erreurModif'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['erreurModif']);
        }
        ?>
    </span>
    <?php
} else {
    ?>
    <span class="messageConfirmation">
        <?php
        if (!empty($_SESSION['succesAjout'])) {
            echo "<br>" . htmlspecialchars($_SESSION['succesAjout'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['succesAjout']);
        }
        ?>
    </span>
    <span class="messageErreur">
        <?php
        if (!empty($_SESSION['erreurAjout'])) {
            echo htmlspecialchars($_SESSION['erreurAjout'], ENT_QUOTES, 'UTF-8');
            unset($_SESSION['erreurAjout']);
        }
        ?>
    </span>
    <?php
}


