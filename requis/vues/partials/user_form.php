<form method="post">

    <!-- INFORMATIONS PERSONNELLES !-->
    <fieldset>
        <legend>
            <strong>INFORMATIONS PERSONNELLES</strong>
        </legend>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="nom">*Nom :</label>
                <input id="nom" type="text" class="form-control" name="nom" placeholder="Nom" value="<?php echo (isset($res)) ? getOldValue($res['NOM']) : ''; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label>*Prénom :</label>
                <input type="text" class="form-control" name="prenom" placeholder="Prénom" value="<?php echo (isset($res)) ? getOldValue($res['PRENOM']) : ''; ?>" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>*Email :</label>
                <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="<?php echo (isset($res)) ? getOldValue($res['EMAIL']) : ''; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label>Mot de passe :</label>
                <input id="email" type="password" class="form-control"  placeholder="mot de passe" value="">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>*Téléphone Fixe :</label>
                <input type="text" class="form-control" name="telFixe" placeholder="Téléphone fixe" value="<?php echo (isset($res)) ? getOldValue($res['TELEPHONEFIXE']) : ''; ?>" required> 
            </div>
            <div class="form-group col-md-6">
                <label>Téléphone Portable :</label>
                <input type="text" class="form-control" name="telPort" placeholder="Téléphone portable" value="<?php echo (isset($res)) ? getOldValue($res['TELEPHONEMOBILE']) : ''; ?>"> 
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Date de Naissance:</label>
                <input type="text" class="form-control" name="dateNaiss" placeholder="Date de naissance" value="<?php echo (isset($res)) ? getOldValue($res['DATENAISSANCE']) : ''; ?>">
            </div>
            <div class="form-group col-md-6">
                <label>Lieu de Naissance :</label>
                <input type="text" class="form-control" name="lieuNaiss" placeholder="Lieu de naissance" value="<?php echo (isset($res)) ? getOldValue($res['LIEUNAISSANCE']) : ''; ?>">
            </div>
        </div>
    </fieldset>

    <hr>
    <!-- INFORMATIONS SUR L'EMPLOI !-->
    <fieldset>
        <legend>
            <strong>INFORMATIONS SUR L'EMPLOI</strong>
        </legend>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>*Date d'arrivée :</label>
                <input type="text" class="form-control" name="dateArrive" placeholder="Date d'arrivée" value="<?php echo (isset($res)) ? getOldValue($res['DATEARRIVE']) : ''; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label>Bureau :</label>
                <input type="text" class="form-control" name="bureau" placeholder="Bureau" value="<?php echo (isset($res)) ? getOldValue($res['BUREAU']) : ''; ?> ">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Bâtiment :</label>
                <input type="text" class="form-control" name="batiment" placeholder="Bâtiment" value="<?php echo (isset($res)) ? getOldValue($res['BATIMENT']) : ''; ?>">
            </div>
            <div class="form-group col-md-6">
                <label>Numéro de contrat :</label>
                <input type="text" class="form-control" name="noContrat" placeholder="Numéro de contrat" value="<?php echo (isset($res)) ? getOldValue($res['NOCONTRAT']) : ''; ?>">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Tutelle :</label>
                <input type="text" class="form-control" name="tutelle" placeholder="Tutelle" value="<?php echo (isset($res)) ? getOldValue($res['TUTELLE']) : ''; ?> ">
            </div>
            <div class="form-group col-md-6">
                <label>Date de départ :</label>
                <input type="text" class="form-control" name="dateDepart" placeholder="Date de départ" value="<?php echo (isset($res)) ? getOldValue($res['DATEDEPART']) : ''; ?>">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Pôle : </label>
                 <?php //print_r($resListePoles); ?>
                <select name="pole">
                    <option>Aucun</option>
                    <?php
                    foreach ($resListePoles as $r) {
                        ?>
                        <option value="<?php (getOldValue($res['CODEPOLE']) == $r['CODEPOLE']) ? 'selected' : ''   ?>"><?php echo $r['CODEPOLE']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </fieldset>

    <!-- BOUTONS !-->
    <div class="form-row">
        <div class="form-group col-md-2">
            <?php if ($action == "modifier"): ?>
                <input type="hidden" name="modifier" value=1>
                <input type="hidden" name="noPersonne" value=<?php echo (isset($res)) ? getOldValue($res['NOPERSONNE']) : ''; ?>>
                <button class="btn btn-primary btn-block" type="submit">Modifier</button>
            <?php else: ?>
                <button class="btn btn-primary btn-block" type="submit">Enregistrer</button>
            <?php endif; ?>

        </div>
        <div class="form-group col-md-2">
            <a href="index.php?page=gestion" class="btn btn-secondary btn-block">Retour</a>
        </div>

    </div>

</form>