<?php
	/*
		Fichier de configuration de l'application de gestion des comptes du GeePs.
	*/

	/*
		Cette constante définit l'environnement de déploiement de l'application.
		Pour autoriser uniquement les connexions HTTPS : écrire "production" à la place de "développement".
	*/
	define("ENVIRONNEMENT", "développement");
	
        //Bdd
	// Nom de domaine du site. Il est utilisé pour la redirection HTTPS.
	define("DOMAINE", "localhost/gestiongeeps/");

	// Cette constante contient les paramètres de la BDD
	define("BDD", "mysql:host=localhost;dbname=gestioncomptegeeps;charset=utf8");
	
	// Cette constante contient le nom d'utilisateur de la BDD
	define("BDD_UTILISATEUR", "root");

	// Cette constante contient le mot de passe de la BDD
	define("BDD_MOTDEPASSE", "");
        
        //LDAP
        define("LDAP_HOST", "ldap://127.0.0.1:389");
        define("LDAP_RDN", "cn=admin,dc=localhost");
        define("LDAP_MDP", "admin");


?>