<?php
namespace App;
/*
Fichier de gestion des cookies de sessions php.
- L'option 'session.cookie_httponly' permet de rendre les cookies de sessions inaccessibles en dehors des connexions HTTP.
- L'option 'session.cookie_secure' permet de rendre les cookies de sessions inaccessibles en dehors des connexions HTTPS. Cette option est conditionnée par une constante.
- La fonction 'session_regenerate_id(true)' permet de changer l'id du cookie de session à chaque chargement de la page pour éviter les usurpations de session (Session Fixation).
- Le token de session permet de vérifier que l'utilisateur a rempli le formulaire de connexion pour éviter les usurpations d'identité de type CSRF (Cross Site Request Forgery).
*/

class Session {

    public $utilisateurConnecte = false;

    public function __construct() {
        $this->redirectHttps();
        $this->startSession();
        $this->changeSessionId();
        $this->protectCSRF();
    }

    private function redirectHttps() {
        // Si l'application est déployée en production, alors on force la redirection HTTPS.
        if (ENVIRONNEMENT == "production") {
            if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
                header($_SERVER["SERVER_PROTOCOL"] . ' 301 Moved Permanently');
                header('Location: ' . 'https://' . DOMAINE . basename($_SERVER['SCRIPT_NAME']));
                exit;
            }
        }
    }

    private function startSession() {
        // Démarrage et paramétrage de la session.
        ini_set('session.cookie_httponly', 1);
        if (ENVIRONNEMENT == "production") {
            ini_set('session.cookie_secure', 1);
        }
        session_start();
    }

    private function changeSessionId() {
        // Utilisateurs connectés : changement de l'id de leur session pour éviter les usurpations de session, et gestion de la déconnexion.
        if (empty($_SESSION['login']) === false) {
            session_regenerate_id(true);
            $this->utilisateurConnecte = true;
            if (empty($_GET['dc']) === false && $_GET['dc'] == 1 && empty($_POST['login']) === true) {
                session_unset();
                session_destroy();
                header('location: index.php');
                exit;
            }
        }
    }

    private function protectCSRF() {
        // Protection CSRF.
        if (empty($_SESSION['token']) === false) {
            if (empty($_POST['token']) === false) {
                if ($_POST['token'] != $_SESSION['token']) {
                    session_unset();
                    session_destroy();
                    header('location: index.php');
                    exit;
                }
            }
            unset($_SESSION['token']);
        }
    }

}

?>