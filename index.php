<?php

require 'vendor/autoload.php';
require_once 'requis/config.php';

use League\Csv\Writer;
use App\Session;
use App\BddClass;
use App\LdapClass;

$session = new Session();
$bdd = new BddClass();
$ldap = new LdapClass();

require_once 'requis/header.php';



if ($session->utilisateurConnecte === true) {

    switch ($_GET['page']) {

        case "gestion":
            if (!empty($_POST['supprimer']) && !empty($_POST['noPersonne'])) {
                $bdd->supprimerPersonne();
                $ldap->supprimerPersonneLdap();
            }
            $resInfosCompte = $bdd->infosCompte();
            $resListePersonnes = $bdd->listePersonnes();
            require_once 'requis/vues/gestion.php';
            break;

        case "compte":
            $resInfosCompte = $bdd->infosCompte();
            if (empty($_POST['login']) === false && empty($_POST['nom']) === false && empty($_POST['prenom']) === false && empty($_POST['email']) === false && empty($_POST['mdp1']) === false && empty($_POST['mdp2']) === false) {
                if ($_POST['mdp1'] == $_POST['mdp2']) {
                    if ($bdd->modifierCompte() === true) {
                        $_SESSION['succesCompte'] = "La mise à jour des informations a été effectuée avec succès.";
                        $resInfosCompte = $bdd->infosCompte();
                    } else {
                        $_SESSION['erreurCompte'] = "Une erreur est survenue lors de la mise à jour des informations.";
                    }
                    header('location: index.php?page=compte');
                    exit;
                } else {
                    $_SESSION['erreurCompte'] = "Erreur : les deux champs du mot de passe ne sont pas identiques";
                }
            }
            $resListePoles = $bdd->listePoles();
            require_once 'requis/vues/compte.php';
            break;

        case "personne":
            if (!empty($_POST['modifier']) && !empty($_POST['noPersonne'])) {
                $resInfosPersonne = $bdd->infosPersonne();

                if (!empty($resInfosPersonne)) {
                    $action = "modifier";
                    if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['telFixe']) && !empty($_POST['dateArrive'])) {
                        if ($bdd->modifierPersonne() && $ldap->modifierPersonneLdap()) {
                            $_SESSION['succesModif'] = "La mise à jour des informations a été effectuée avec succès.";
                            $resInfosPersonne = $bdd->infosPersonne();
                        } else {
                            $_SESSION['erreurModif'] = "Erreur : les dates doivent être au format jj/mm/aaaa.";
                        }
                    }
                }
            } else {
                $action = "ajouter";
                if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['telFixe']) && !empty($_POST['dateArrive'])) {
                    if ($bdd->ajouterPersonne() && $ldap->ajouterPersonneLdap()) {
                        $_SESSION['succesAjout'] = "Les informations ont été enregistrées avec succès.";
                        $resInfosPersonne = $bdd->infosPersonne();
                    } else {
                        $_SESSION['erreurAjout'] = "Erreur : les dates doivent être au format jj/mm/aaaa.";
                    }
                }
            }
            $resListePoles = $bdd->listePoles();

            require_once 'requis/vues/personne.php';
            break;


        case "importer":
            ob_end_clean();
            $sth = $bdd->infosPersonne();
            $csv = Reader::createFromFileObject(new SplTempFileObject());
            $csv->insertOne(['NOPERSONNE', 'NOM', 'PRENOM', 'EMAIL', 'DATENAISSANCE', 'LIEUNAISSANCE', 'TELEPHONEFIXE', 'TELEPHONEMOBILE', 'BUREAU', 'BATIMENT', 'NOCONTRAT', 'TUTELLE', 'DATEARRIVE', 'DATEDEPART']);
            $csv->insertAll($sth);
            $csv->output('export.csv');
            //var_dump($resInfosPersonne);
            die;
            break;

        case "export_personne":
            ob_end_clean();
            $sth = $bdd->infosPersonne();
            $array_clean = array();
            foreach ($sth as $k => $item) {
                foreach ($item as $key => $pers) {
                    if (!is_int($key)) {
                        $array_clean[$k][$key] = $pers;
                    }
                }
            }
            $csv = Writer::createFromFileObject(new SplTempFileObject());
            $csv->insertOne(['NOPERSONNE', 'NOM', 'PRENOM', 'EMAIL', 'DATENAISSANCE', 'LIEUNAISSANCE', 'TELEPHONEFIXE', 'TELEPHONEMOBILE', 'BUREAU', 'BATIMENT', 'NOCONTRAT', 'TUTELLE', 'DATEARRIVE', 'DATEDEPART']);
            $csv->insertAll($array_clean);
            $csv->output('export.csv');
            //var_dump($resInfosPersonne);
            die;
            break;

        case "exporter":
            ob_end_clean();
            $sth = $bdd->listePersonnes();
            $array_clean = array();
            foreach ($sth as $k => $item) {
                foreach ($item as $key => $pers) {
                    if (!is_int($key)) {
                        $array_clean[$k][$key] = $pers;
                    }
                }
            }

            $csv = Writer::createFromFileObject(new SplTempFileObject());
            $csv->insertOne(['NOPERSONNE', 'NOM', 'PRENOM', 'EMAIL', 'DATENAISSANCE', 'LIEUNAISSANCE', 'TELEPHONEFIXE', 'TELEPHONEMOBILE', 'BUREAU', 'BATIMENT', 'NOCONTRAT', 'TUTELLE', 'DATEARRIVE', 'DATEDEPART']);
            $csv->insertAll($array_clean);
            $csv->output('export.csv');
            die;
            break;

        default:
            header('location: index.php?page=gestion');
            exit;
    }
} else {
    if (isset($_POST['login'])) {
        if ($bdd->identification($_POST['login'], $_POST['mdp']) === true) {
            header('location: index.php?page=gestion');
            exit;
        } else {
            $_SESSION['erreurIdentification'] = "Login et/ou mot de passe erroné(s)";
        }
    }
    require_once 'requis/vues/connexion.php';
}
?>